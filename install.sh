#!/bin/bash

# https://openwrt.org/docs/guide-user/additional-software/imagebuilder#archmanjaro
# https://openwrt.org/docs/guide-developer/toolchain/install-buildsystem#archmanjaro

sed -i 's,#ParallelDownloads,ParallelDownloads,g' /etc/pacman.conf
pacman -Syu --noconfirm --noprogressbar \
asciidoc \
autoconf \
automake \
base-devel \
bash \
bin86 \
binutils \
bison \
boost \
bzip2 \
fakeroot \
file \
findutils \
flex \
gawk \
gcc \
gettext \
git \
grep \
groff \
gzip \
help2man \
intltool \
libelf \
libusb \
libtool \
libxslt \
m4 \
make \
ncurses \
openssl \
patch \
perl-extutils-makemaker \
pkgconf \
python \
rsync \
sharutils \
sed \
texinfo \
time \
unzip \
util-linux \
wget \
which \
zlib
