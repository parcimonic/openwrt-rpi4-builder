#!/bin/bash

OWRT_VERSION=21.02.3
OWRT_PROFILE=rpi-4
OWRT_PACKAGES="bcm27xx-userland \
cgi-io \
curl \
iperf3 \
iptables-mod-conntrack-extra \
iptables-mod-ipopt \
kmod-crypto-hash \
kmod-crypto-kpp \
kmod-crypto-lib-blake2s \
kmod-crypto-lib-chacha20 \
kmod-crypto-lib-chacha20poly1305 \
kmod-crypto-lib-curve25519 \
kmod-crypto-lib-poly1305 \
kmod-ifb \
kmod-ipt-conntrack-extra \
kmod-ipt-ipopt \
kmod-ipt-raw \
kmod-libphy \
kmod-mii \
kmod-sched-cake \
kmod-sched-core \
kmod-udptunnel4 \
kmod-udptunnel6 \
kmod-usb-net \
kmod-usb-net-asix \
kmod-usb-net-asix-ax88179 \
kmod-usb-net-rtl8152 \
kmod-usb3 \
kmod-wireguard \
libcurl4 \
libiwinfo-lua \
liblucihttp-lua \
liblucihttp0 \
libmnl0 \
libpcap1 \
libubus-lua \
losetup \
lua \
luci \
luci-app-firewall \
luci-app-opkg \
luci-app-sqm \
luci-app-wireguard \
luci-base \
luci-lib-base \
luci-lib-ip \
luci-lib-jsonc \
luci-lib-nixio \
luci-mod-admin-full \
luci-mod-network \
luci-mod-status \
luci-mod-system \
luci-proto-ipv6 \
luci-proto-ppp \
luci-proto-wireguard \
luci-ssl \
luci-theme-bootstrap \
px5g-wolfssl \
rpcd \
rpcd-mod-file \
rpcd-mod-iwinfo \
rpcd-mod-luci \
rpcd-mod-rrdns \
sqm-scripts \
tc-mod-iptables \
tc-tiny \
tcpdump \
uhttpd \
uhttpd-mod-ubus \
wireguard-tools"

wget "https://downloads.openwrt.org/releases/${OWRT_VERSION}/targets/bcm27xx/bcm2711/openwrt-imagebuilder-${OWRT_VERSION}-bcm27xx-bcm2711.Linux-x86_64.tar.xz"
tar -J -x -f "openwrt-imagebuilder-${OWRT_VERSION}-bcm27xx-bcm2711.Linux-x86_64.tar.xz"

cd "openwrt-imagebuilder-${OWRT_VERSION}-bcm27xx-bcm2711.Linux-x86_64" || exit
time make image PROFILE="${OWRT_PROFILE}" PACKAGES="${OWRT_PACKAGES}"
