# RPi4 OpenWrt build

## Archival notice

The official [firmware selector](https://firmware-selector.openwrt.org) now has an option to build a custom image with custom packages

---

OpenWrt has an [official download](https://downloads.openwrt.org/releases/21.02.1/targets/bcm27xx/bcm2711/) for the RPi4, but if you need to use additional drivers (like WireGuard and additional USB ethernet interfaces), updating to a new version can be painful.

This repository provides a Vagrantfile that automates the building of a new image with all needed drivers that can be used to upgrade your OpenWrt install.

## Preparing for build

The VM has been tested on an Arch Linux host using libvirt as the virtualization provider. Install the vagrant-libvirt plugin as described in the [documentation](https://wiki.archlinux.org/title/Vagrant#vagrant-libvirt)

Modify the CPU and memory configuration of the VM if needed. With the current configuration, the OpenWrt build takes ~4 minutes.

Get a list of packages that you want to include in the build and update the `OWRT_PACKAGES` variable in `build.sh`. The current list of packages will install WireGuard and drivers for two ethernet gigabit adapters.

Some tips on how to get the list of packages:

- The [official manifest](https://downloads.openwrt.org/releases/21.02.1/targets/bcm27xx/bcm2711/openwrt-21.02.1-bcm27xx-bcm2711.manifest) file has the list of packages included in a typical RPi4 install. Note that the image provided by the image builder will *not* include packages like LuCi (the web interface).

- Use the command `opkg list-installed` to get the current list of installed packages of an already running RPi4 install. Use the parameter `--strip-abi` to get the same list without ABI versions. See the [documentation](https://openwrt.org/docs/guide-user/additional-software/imagebuilder#selecting_packages) for more information.

## Building

Just run `vagrant up` :D

After the build is finished, you can get the resulting files inside of the VM at `~/openwrt-imagebuilder-<version>-bcm27xx-bcm2711.Linux-x86_64/bin/targets/bcm27xx/bcm2711`. A manifest file will be there with all included packages, so you can compare with what you have from the previous section and adjust `OWRT_PACKAGES` if needed.

To get the files from the VM, you can use `scp`. To get SSH connection details, use `vagrant ssh-config`, which will print something like:

```shell
Host openwrt
  HostName 192.0.0.2 <- take note of the IP address
  User vagrant
  Port 22
  UserKnownHostsFile /dev/null
  StrictHostKeyChecking no
  PasswordAuthentication no
  IdentityFile /home/someplace/.vagrant/machines/openwrt/libvirt/private_key
  IdentitiesOnly yes
  LogLevel FATAL
```

Then, you can run `scp vagrant@192.0.0.2:~/openwrt-imagebuilder-21.02.1-bcm27xx-bcm2711.Linux-x86_64/bin/targets/bcm27xx/bcm2711/<image-name-here> .` to copy the file from the VM to your current working directory. The user password is `vagrant`. If you get a prompt asking to about authenticity of the host and if you want to continue connecting, answer `yes`.

Remember to always make backups before proceeding with the OpenWrt upgrade. Ideally, create an image of the SD card for the current working install before atempting any upgrades.

## Alternative build with Docker

As suggested [here](https://teddit.net/r/openwrt/comments/swh5se/my_small_helper_vmscripts_to_build_the_rpi4_image/hysqld0/#c), there is a [project on GitHub](https://github.com/jandelgado/lede-dockerbuilder) that does the same thing but with Docker.
